<?php

/**
 * Fired during plugin activation
 *
 * @link       https://developeratx.com/
 * @since      1.0.0
 *
 * @package    Woo_Storewide_Sale
 * @subpackage Woo_Storewide_Sale/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Woo_Storewide_Sale
 * @subpackage Woo_Storewide_Sale/includes
 * @author     Craig Williams <developeratx@gmail.com>
 */
class Woo_Storewide_Sale_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
        update_option( 'devatx_woo_storewide_sale_options', array(
            'sale_type' => '',
            'sale_amount' => '',
            'sale_start_date' => '',
            'sale_end_date' => '',
        ), true );
	}

}
