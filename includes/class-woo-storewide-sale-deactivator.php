<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://developeratx.com/
 * @since      1.0.0
 *
 * @package    Woo_Storewide_Sale
 * @subpackage Woo_Storewide_Sale/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Woo_Storewide_Sale
 * @subpackage Woo_Storewide_Sale/includes
 * @author     Craig Williams <developeratx@gmail.com>
 */
class Woo_Storewide_Sale_Deactivator
{

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function deactivate()
    {
        delete_option('devatx_woo_storewide_sale_options');
    }

}
