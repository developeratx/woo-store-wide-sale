<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://developeratx.com/
 * @since             1.0.0
 * @package           Woo_Storewide_Sale
 *
 * @wordpress-plugin
 * Plugin Name:       Woo Storewide Sale
 * Plugin URI:        https://developeratx.com/woo-storewide-sale
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Craig Williams
 * Author URI:        https://developeratx.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       woo-storewide-sale
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-woo-storewide-sale-activator.php
 */
function activate_woo_storewide_sale() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woo-storewide-sale-activator.php';
	Woo_Storewide_Sale_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-woo-storewide-sale-deactivator.php
 */
function deactivate_woo_storewide_sale() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woo-storewide-sale-deactivator.php';
	Woo_Storewide_Sale_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_woo_storewide_sale' );
register_deactivation_hook( __FILE__, 'deactivate_woo_storewide_sale' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-woo-storewide-sale.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_woo_storewide_sale() {

	$plugin = new Woo_Storewide_Sale();
	$plugin->run();

}
run_woo_storewide_sale();
