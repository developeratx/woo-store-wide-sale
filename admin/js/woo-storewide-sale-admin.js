(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-specific JavaScript source
	 * should reside in this file.
	 *
	 * Note that this assume you're going to use jQuery, so it prepares
	 * the $ function reference to be used within the scope of this
	 * function.
	 *
	 * From here, you're able to define handlers for when the DOM is
	 * ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * Or when the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and so on.
	 *
	 * Remember that ideally, we should not attach any more than a single DOM-ready or window-load handler
	 * for any particular page. Though other scripts in WordPress core, other plugins, and other themes may
	 * be doing this, we should try to minimize doing that in our own work.
	 */
    $( document ).ready(function() {

        $('#datetimepicker1').datetimepicker({
            format: 'yyyy-MM-dd',
            pickDate: true,
            pickTime: false,
            language: 'en_US'
        });
        $('#datetimepicker2').datetimepicker({
            format: 'yyyy-MM-dd',
            pickDate: true,
            pickTime: false,
            language: 'en_US'
        });

        $('#devatx-storewide-sale-save-settings').on('click', function(e){
            console.log('#devatx-storewide-sale-save-settings');
            $('#devatx-storewide-sale-form').submit();
        });

        $('#devatx-storewide-sale-clear-settings').on('click', function(e){
            console.log('#devatx-storewide-sale-clear-settings');
            $('#mode').val('clear');
            $('#devatx-storewide-sale-form').submit();
        });

    });



})( jQuery );
