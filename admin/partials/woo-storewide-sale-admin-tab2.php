<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://developeratx.com/
 * @since      1.0.0
 *
 * @package    Woo_Storewide_Sale
 * @subpackage Woo_Storewide_Sale/admin/partials
 * @file       woo-storewide-sale/admin/partials/woo-storewide-sale-admin-tab2.php
 */

?>


<div class="row">
    <div class="col-md-12" class="header-row">
        <h1>WooCommerce Store-wide Sale Settings</h1>

        <div class="header-button">
            <!-- Trigger the modal with a button -->
            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Help
            </button>
        </div>

        <p class="lead">Control your store-wide sales here....</p>


    </div>
</div>
<?php
echo $message;
?>
<br/>
<form class="form-horizontal" role="form" method="post"
      action="/wp-admin/admin.php?page=storewide-sale-settings" id="devatx-storewide-sale-form">
    <input type="hidden" name="mode" id="mode" value="save">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Current Sale</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <td>Product count:</td>
                                <td><?php echo $product_count; ?></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>

                            <tr>
                                <td>Products on sale:</td>
                                <td><?php echo $on_sale_count; ?></td>
                                <td>Not on sale:</td>
                                <td><?php echo $not_on_sale; ?></td>
                            </tr>

                            <tr>
                                <td>Sale start date:</td>
                                <td><?php echo $start_date; ?></td>
                                <td>Sale end date:</td>
                                <td><?php echo $end_date; ?></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
</div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Sale Schedule</h3>
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <td>Type of sale:</td>
                                    <td>
                                        <label class="radio-inline">
                                            <input type="radio" value="percentage"
                                                   name="sale_type" <?php if ($options['sale_type'] == 'percentage') echo ' checked="checked"'; ?>>Percentage</label>
                                        <label class="radio-inline">
                                            <input type="radio" value="fixed"
                                                   name="sale_type" <?php if ($options['sale_type'] == 'fixed') echo ' checked="checked"'; ?>>Fixed
                                            Amount</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Percentage (or amount)</td>
                                    <td>
                                        <input type="text" value="<?php echo $options['sale_amount']; ?>"
                                               class="form-control" id="amount" name="sale_amount"
                                               style="max-width:10em;">
                                    </td>
                                </tr>

                                <tr>
                                    <td>Use friendly prices (.00, .25, .49, .75, .99)</td>
                                    <td>
                                        <input type="checkbox" name="sale_friendly" value="1" <?php if ($options['sale_friendly'] == '1') echo ' checked="checked"'; ?>/>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Start sale:</td>
                                    <td>
                                        <div class="input-append date" id="datetimepicker1">
                                            <input value="<?php echo $options['sale_start_date']; ?>"
                                                   data-format="yyyy-MM-dd hh:mm:ss" type="text" name="sale_start_date">
                                            <span class="add-on">
                                                <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                                                <i data-date-icon="icon-calendar" data-time-icon="icon-time"
                                                   class="icon-calendar" id="xx"></i>
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>End sale:</td>
                                    <td>
                                        <div class="input-append date" id="datetimepicker2">
                                            <input value="<?php echo $options['sale_end_date']; ?>"
                                                   data-format="yyyy-MM-dd hh:mm:ss" type="text" name="sale_end_date">
                                            <span class="add-on">
                                                <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                                                <i data-date-icon="icon-calendar" data-time-icon="icon-time"
                                                   class="icon-calendar"></i>
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="devatx-align-center">
                                        <button type="button" class="btn btn-lg btn-primary"
                                                id="devatx-storewide-sale-save-settings">Save Settings
                                        </button>
                                    </td>
                                </tr>

                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>

                                <tr>
                                    <td colspan="2" class="devatx-align-center">
                                        <button type="button" class="btn btn-lg btn-danger"
                                                id="devatx-storewide-sale-clear-settings">Clear Settings
                                        </button>
                                        <br/>Warning! This will stop any sale that is scheduled or in progress.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div><!-- pbody -->
                    </div>
                </div>
            </div>
</form>


<?php if ($unchanged_count > 0) { ?>


    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Unchanged Items
                    </h3>
                </div>
                <div class="panel-body">
                    <p>
                        <?php
                        if ($unchanged_count == 1) {
                            echo 'This product was left unchanged.';
                        } else {
                            echo "These $unchanged_count products were left unchanged.";
                        }
                        ?>
                    </p>

                    <table class="table table-striped">
                        <thead>
                        <th>SKU</th>
                        <th>Item</th>
                        <th class="storewide_amt_align-right">Your Regular Price</th>
                        <th class="storewide_amt_align-right">Computed Sale Price</th>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($unchanged as $post_id) {
                            $product = $this->_lookup_product($post_id);

                            $sale_price = $this->_compute_one_price($product['_regular_price'], $options);
                            if ($sale_price < 0) {
                                $sign = 'storewide_amt_under';
                                $sale_price = '(' . number_format(-1 * $sale_price, 2) . ')';
                            } else {
                                $sign = 'storewide_amt_over';
                            }
                            echo '<tr>';
                            echo '<td>' . $product['_sku'] . '</td>';
                            echo '<td>' . $product['post_title'] . '</td>';
                            echo '<td class="storewide_amt_align-right">' . $product['_regular_price'] . '</td>';
                            echo '<td class="storewide_amt_align-right '.$sign.'">' . $sale_price . '</td>';

                            echo '<tr>';
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>

</div><!-- /.container -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">WooCommerce Store-wide Sale Help</h4>
            </div>
            <div class="modal-body">
                <h3>How to stop a sale immediately</h3>
                <p>Click on the red button labelled "Clear Immediately." All sale prices site-wide will be cleared,
                    including prices set manually.
                </p>
                <h3>How to set a sale immediately</h3>
                <ul>
                    <li>
                        Select the type of sale and enter the amount of discount to apply
                    </li>
                    <li>
                        <strong>Percentage:</strong> Applies a percentage discount (under 100%) to all items.
                    </li>
                    <li>
                        <strong>Fixed:</strong> Deducts a fixed amount from every item.
                    </li>
                    <li>
                        Enter an amount or percentage to deduct.  Do not enter symbols ($%) - just numbers.
                    </li>
                    <li>
                        For immediate sales, leave both date fields blank.
                    </li>
                </ul>

                <h3>How to set a future sale</h3>
                <p>Set up the sale type and amount as above, then set the start and end date for the sale.</p>
                <h3>Why were some items not included in the sale?</h3>
                <p>As a safeguard against setting prices too low or even below zero, some prices won't
                    be changed when you apply a sale. Percentage sales will always be applied to every item on the site,
                    but fixed amount sales sometimes won't. When a fixed sale price is calculated to be below zero
                    or less than the fixed amount, the sale price will not be used.
                </p>
                <p>
                    You will see a message when some items are left unchanged.  You can review the list of unchanged
                    products at the bottom of the page.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>