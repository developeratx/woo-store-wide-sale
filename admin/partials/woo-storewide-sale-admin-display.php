<?php
/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://developeratx.com/
 * @since      1.0.0
 *
 * @package    Woo_Storewide_Sale
 * @subpackage Woo_Storewide_Sale/admin/partials
 * @file       woo-storewide-sale/admin/partials/woo-storewide-sale-admin-display
 */
?>
<br />
<div>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>

    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">
            <?php include(plugin_dir_path(__FILE__) . 'woo-storewide-sale-admin-tab2.php'); ?>
        </div>

    </div>

</div>