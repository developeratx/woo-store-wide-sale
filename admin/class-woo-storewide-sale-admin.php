<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://developeratx.com/
 * @since      1.0.0
 *
 * @package    Woo_Storewide_Sale
 * @subpackage Woo_Storewide_Sale/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Woo_Storewide_Sale
 * @subpackage Woo_Storewide_Sale/admin
 * @author     Craig Williams <developeratx@gmail.com>
 */
class Woo_Storewide_Sale_Admin
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $plugin_name The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $version The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string $plugin_name The name of this plugin.
     * @param      string $version The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Woo_Storewide_Sale_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Woo_Storewide_Sale_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        if ($_GET['page'] == 'storewide-sale-settings') {
            wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/woo-storewide-sale-admin.css', array(), $this->version, 'all');
            wp_register_style('bootstrap-css', plugin_dir_url(__FILE__) . 'css/bootstrap.min.css', array(), '3.0.1', 'all');
            wp_enqueue_style('bootstrap-css');
            wp_enqueue_style('bootstrap=-picker-css', plugin_dir_url(__FILE__) . 'css/bootstrap-datetimepicker.min.css', array(), $this->version, 'all');

        }

    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Woo_Storewide_Sale_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Woo_Storewide_Sale_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        if ($_GET['page'] == 'storewide-sale-settings') {

            wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/woo-storewide-sale-admin.js', array('jquery'), $this->version, true);
            wp_register_script('bootstrap-js', plugin_dir_url(__FILE__) . 'js/bootstrap.min.js', array('jquery'), '3.0.1', true);
            wp_enqueue_script('bootstrap-js');
            wp_enqueue_script('bootstrap-picker-js', plugin_dir_url(__FILE__) . 'js/bootstrap-datetimepicker.min.js', array('jquery', 'bootstrap-js'), $this->version, true);
        }
    }


    public function storewide_sale_menu()
    {
        add_submenu_page('woocommerce', 'Storewide Sale Settings', 'Storewide Sale', 'manage_options', 'storewide-sale-settings', array($this, 'storewide_sale_options'));
    }


    public function storewide_sale_options()
    {
        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.'));
        }

        $unchanged = array();
        $message = '';

        if (isset($_POST['mode'])) {
            $options = $_POST;
            if (!in_array($options['sale_type'], array('fixed', 'percentage'))) {
                $options['sale_type'] = 'percentage';
            }
            $options['sale_friendly'] = isset($options['sale_friendly']) ? true : false;
            $options['sale_amount'] = sprintf("%.2f", floatval($options['sale_amount']));

            // Need valid dates in both fields or neither
            $options['sale_start_date'] = date('Y-m-d', strtotime($options['sale_start_date']));
            if ($options['sale_start_date'] == false || $options['sale_start_date'] == '1970-01-01') {
                $options['sale_start_date'] = $options['sale_end_date'] = '';
            }

            $options['sale_end_date'] = date('Y-m-d', strtotime($options['sale_end_date']));
            if ($options['sale_start_date'] == false || $options['sale_start_date'] == '1970-01-01') {
                $options['sale_start_date'] = $options['sale_end_date'] = '';
            }

            update_option('devatx_woo_storewide_sale_options', $options);

            if ($_POST['mode'] === 'clear') {
                update_option('devatx_woo_storewide_sale_options', array(
                    'sale_type' => '',
                    'sale_amount' => '0',
                    'sale_start_date' => '',
                    'sale_end_date' => ''
                ));
                $this->_clear_all_sale_prices();
            } else {
                // Update sale dates and sale prices
                $future = $this->_date_is_in_the_future($options['sale_start_date']);

                switch ($options['sale_type']) {
                    case 'percentage':
                        $unchanged = $this->_apply_percentage_discount($options);
                        break;
                    case 'fixed':
                        $unchanged = $this->_apply_fixed_discount($options);
                        break;
                    default:
                        update_option('devatx_woo_storewide_sale_options', array(
                            'sale_type' => '',
                            'sale_amount' => '0',
                            'sale_start_date' => '',
                            'sale_end_date' => ''
                        ));
                        $this->_clear_all_sale_prices();
                }
            }

            $message = '<div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Your settings have been saved.
                </div>';
        }

        $options = get_option('devatx_woo_storewide_sale_options');

        $future = $this->_date_is_in_the_future($options['sale_start_date']);

        if ($future && $options['sale_start_date'] != '') {
            $message .= '<div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                The current sale is set to start in the future.
                </div>';
        }

        if ($options['sale_start_date'] == '') {
            $start_date = ' - ';
        } else {
            $start_date = $options['sale_start_date'];
        }

        if ($options['sale_end_date'] == '') {
            $end_date = ' - ';
        } else {
            $end_date = $options['sale_end_date'];
        }

        $product_count = $this->_get_product_count();
        $on_sale_count = $this->_get_products_on_sale_count();
        $not_on_sale = max(0, $product_count - $on_sale_count);
        $unchanged_count = count($unchanged);

        if ($unchanged_count > 0) {
            $message .= '<div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Some of your products were not added to the sale. Check the list at the bottom of the page.
                </div>';
        }

        include(plugin_dir_path(__FILE__) . 'partials/woo-storewide-sale-admin-display.php');
    }


    private function _apply_fixed_discount($options)
    {
        $amount = floatval($this->_clean_money($options['sale_amount']));

        if ($amount <= 0) {
            return array();
        }

        $friendly = $options['sale_friendly'];
        $this->_clear_all_sale_prices();
        $regular_prices = $this->_get_regular_prices();
        $not_updated = array();
        $future = $this->_date_is_in_the_future($options['sale_start_date']);

        foreach ($regular_prices as $ID => $price_data) {
            $regular_price = floatval($this->_clean_money($price_data['_regular_price']));
            $sale_price = floatval($regular_price) - $amount;

            if ($friendly) {
                $sale_price = floatval($this->_friendly($sale_price));
            }

            // Safeguard - No negative or zero prices
            if ($sale_price <= 0 || $sale_price >= $regular_price) {
                $sale_price = '';
                $not_updated[] = $ID;
                $this->_set_sale_price($ID, $sale_price);
                $this->_set_price_price($ID, $regular_price);
                $this->_clear_sale_dates($ID);
            } else {
                $sale_price = sprintf("%.2f", floatval($sale_price));
                $regular_price = sprintf("%.2f", floatval($regular_price));
                if ($future) {
                    $this->_set_sale_price($ID, $sale_price);
                    $this->_set_price_price($ID, $regular_price);
                    $this->_apply_sale_dates($ID, $options['sale_start_date'], $options['sale_end_date']);
                } else {
                    $this->_set_sale_price($ID, $sale_price);
                    $this->_set_price_price($ID, $sale_price);
                    $this->_clear_sale_dates($ID);
                }
            }
        }

        return $not_updated;
    }


    private function _apply_percentage_discount($options)
    {
        $percentage = floatval($options['sale_amount']) / 100;
        $friendly = $options['sale_friendly'];

        if ($percentage <= 0 || $percentage >= 1) {
            return array();
        }

        $this->_clear_all_sale_prices();
        $regular_prices = $this->_get_regular_prices();
        $not_updated = array();
        $future = $this->_date_is_in_the_future($options['sale_start_date']);

        foreach ($regular_prices as $ID => $price_data) {
            $regular_price = floatval($this->_clean_money($price_data['_regular_price']));
            $discount = $regular_price * $percentage;
            $sale_price = $regular_price - $discount;

            if ($friendly) {
                $sale_price = floatval($this->_friendly($sale_price));
            }

            // Safeguard - No negative or zero prices
            if ($sale_price <= 0 || $sale_price >= $regular_price) {
                $sale_price = '';
                $not_updated[] = $ID;
                $this->_set_sale_price($ID, $sale_price);
                $this->_set_price_price($ID, $regular_price);
                $this->_clear_sale_dates($ID);
            } else {
                $sale_price = sprintf("%.2f", floatval($sale_price));
                $regular_price = sprintf("%.2f", floatval($regular_price));
                if ($future) {
                    $this->_set_sale_price($ID, $sale_price);
                    $this->_set_price_price($ID, $regular_price);
                    $this->_apply_sale_dates($ID, $options['sale_start_date'], $options['sale_end_date']);
                } else {
                    $this->_set_sale_price($ID, $sale_price);
                    $this->_set_price_price($ID, $sale_price);
                    $this->_clear_sale_dates($ID);
                }
            }
        }

        return $not_updated;
    }

    private function _set_sale_price($post_id, $price)
    {
        global $wpdb;

        if ($price !== '') {
            $price = $this->_clean_money($price);
        }

        $sql = "UPDATE $wpdb->postmeta";
        $sql .= " SET meta_value='$price'";
        $sql .= " WHERE meta_key = '_sale_price' ";
        $sql .= " AND post_id = '$post_id'";
        $wpdb->query($sql);
    }

    private function _set_price_price($post_id, $price)
    {
        global $wpdb;

        if ($price !== '') {
            $price = $this->_clean_money($price);
        }

        $sql = "UPDATE $wpdb->postmeta";
        $sql .= " SET meta_value='$price'";
        $sql .= " WHERE meta_key = '_price' ";
        $sql .= " AND post_id = '$post_id'";
        $wpdb->query($sql);
    }

    private function _friendly($amt)
    {
        $amt = strval($amt);
        $amt = str_replace(',', '', $amt);
        $parts = explode('.', $amt, 2);
        $decimal = $parts[1];
        if ($decimal == 0) {
            return sprintf("%.2f", $amt);
        }
        if ($decimal < 26) {
            $amt = $parts[0] + 0.25;
            return sprintf("%.2f", $amt);
        }
        if ($decimal < 51) {
            $amt = $parts[0] + 0.49;
            return sprintf("%.2f", $amt);
        }
        if ($decimal < 76) {
            $amt = $parts[0] + 0.75;
            return sprintf("%.2f", $amt);
        }
        $amt = $parts[0] + 0.95;
        return sprintf("%.2f", $amt);
    }


    private function _compute_one_price($regular, $options)
    {
        $regular = $this->_clean_money($regular);

        switch ($options['sale_type']) {
            case 'percentage':
                $percentage = floatval($options['sale_amount']) / 100;
                $regular_price = floatval($this->_clean_money($regular));
                $discount = $regular_price * $percentage;
                $sale_price = $regular_price - $discount;
                echo "$percentage $regular_price $discount $sale_price \n\n";
                break;
            case 'fixed':
                $sale_price = sprintf("%.2f", floatval($regular) - $options['sale_amount']);
                break;
        }

        if ($options['sale_friendly']) {
            return $this->_friendly($sale_price);
        }

        return $sale_price;
    }

    private function _get_product_count()
    {
        global $wpdb;
        $c = $wpdb->get_var("
		  SELECT count(ID)
		  FROM $wpdb->posts
		  WHERE post_status = 'publish'
		  AND post_type = 'product'
	    ");
        return ($c !== null) ? $c : 0;
    }

    private function _get_products_on_sale_count()
    {
        global $wpdb;
        $products = $this->_get_all_product_ids();

        $post_list = implode(',', $products);

        $c = $wpdb->get_var("
          SELECT COUNT(post_id)
          FROM $wpdb->postmeta
          WHERE meta_key = '_sale_price'
          AND meta_value != ''
          AND post_id IN ($post_list)
        ");

        if ($c == null) {
            return 0;
        }

        return intval($c);
    }

    private function _date_is_in_the_future($date, $is_timestamp = false)
    {
        if ($date == '') {
            return false;
        }

        if (!$is_timestamp) {
            $date = strtotime($date);
        }

        $now = time();

        return $date > $now;
    }

    private function _clear_all_sale_prices()
    {
        global $wpdb;

        $regular_prices = $this->_get_regular_prices();
        $products = $this->_get_all_product_ids();
        $post_list = implode(',', $products);

        $sql = "UPDATE $wpdb->postmeta SET meta_value = '' WHERE meta_key = '_sale_price' AND post_id IN ($post_list)";
        $wpdb->query($sql);

        foreach ($regular_prices as $ID => $product) {
            $sql = "UPDATE $wpdb->postmeta SET meta_value = '" . $product['_regular_price'] . "'";
            $sql .= " WHERE meta_key='_price' AND post_id='$ID'";
            $wpdb->query($sql);

            $sql = "UPDATE $wpdb->postmeta SET meta_value = ''";
            $sql .= " WHERE meta_key='_sale_price_dates_from' AND post_id='$ID'";
            $wpdb->query($sql);

            $sql = "UPDATE $wpdb->postmeta SET meta_value = ''";
            $sql .= " WHERE meta_key='_sale_price_dates_to' AND post_id='$ID'";
            $wpdb->query($sql);
        }

        return count($products);
    }

    private function _clean_money($amt)
    {
        $amt = preg_replace('/[^0-9\.]/', '', $amt);
        return sprintf("%.2f", $amt);
    }


    private function _get_regular_prices()
    {
        global $wpdb;
        $products = $this->_get_all_product_ids();
        $post_list = implode(',', $products);

        $sql = "SELECT post_id, meta_value as _regular_price FROM $wpdb->postmeta WHERE meta_key = '_regular_price' AND post_id IN ($post_list)";
        $postmeta = $wpdb->get_results($sql, ARRAY_A);

        $newpostmeta = array();

        foreach ($postmeta as $m) {
            $m['_regular_price'] = $this->_clean_money($m['_regular_price']);
            $newpostmeta[$m['post_id']] = $m;
        }

        return $newpostmeta;

    }


    private function _lookup_product($post_id)
    {
        global $wpdb;

        $p = $wpdb->get_results("SELECT * FROM $wpdb->posts WHERE ID='$post_id'", ARRAY_A);

        if ($p == null) {
            return array();
        }

        $p = $p[0];

        $m = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id = '$post_id'", ARRAY_A);

        foreach ($m as $entry) {
            $k = $entry['meta_key'];
            $p[$k] = $entry['meta_value'];
        }

        return $p;
    }

    private function _apply_sale_dates($post_id, $start, $end)
    {
        global $wpdb;

        // If either sale date is blank, they both should be blank
        if ($start == '' || $end == '') {
            $start = $end = '';
        } else {
            $start = strtotime($start);
            $end = strtotime($end);
        }

        $sql = "UPDATE $wpdb->postmeta SET meta_value = '$start' WHERE meta_key = '_sale_price_dates_from'";
        $sql .= " AND post_id = '$post_id'";
        $wpdb->query($sql);

        $sql = "UPDATE $wpdb->postmeta SET meta_value = '$end' WHERE meta_key = '_sale_price_dates_to'";
        $sql .= " AND post_id = '$post_id'";
        $wpdb->query($sql);

    }

    private function _clear_sale_dates($post_id)
    {
        global $wpdb;

        $sql = "UPDATE $wpdb->postmeta SET meta_value = '' WHERE meta_key = '_sale_price_dates_from'";
        $sql .= " AND post_id = '$post_id'";
        $wpdb->query($sql);

        $sql = "UPDATE $wpdb->postmeta SET meta_value = '' WHERE meta_key = '_sale_price_dates_to'";
        $sql .= " AND post_id = '$post_id'";
        $wpdb->query($sql);

    }

    private function _get_all_product_ids($include_inactive = true)
    {
        global $wpdb;

        $sql = "SELECT ID FROM $wpdb->posts WHERE post_type = 'product'";
        if ($include_inactive) {
            $sql .= " AND post_status = 'publish'";
        }

        $products = $wpdb->get_results($sql, ARRAY_A);

        if ($products == null) {
            return array();
        }

        $a = array();
        if (is_array($products)) {
            foreach ($products as $v) {
                $a[] = $v['ID'];
            }
        }

        return $a;
    }


}
